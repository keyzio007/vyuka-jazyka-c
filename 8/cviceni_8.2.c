#include <stdio.h>

void troj(char c, int n);

int main(void){
    char znak = 'O';
    int radek = 5;

    troj(znak, radek);   
    return 0;
}

void troj(char c, int n){
    int i;
    static int h = 1;
    for (i = 1 ; i <= n ; i++){
        for (int k = 1 ; k <= (n-i) ; k++){
            putchar(' ');
        }
        for (int k = 1; k <= h ; k++){
            putchar(c);
        }
    h += 2;
    putchar('\n');
    }
}