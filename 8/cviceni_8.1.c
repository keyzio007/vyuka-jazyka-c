#include <stdio.h>

void tabulka_mocnin(double x, int n);
int main(){
    tabulka_mocnin(3.2, 5);
    putchar('\n');
    return 0;
}

void tabulka_mocnin(double x, int n){
    int i, k;
    double mezivysledek;
    for (i = 1 ; i <= n ; i++){
        for (mezivysledek = 1,  k = 1;  k <= i; k++){
            mezivysledek *= x;
        }
        printf("%.2f^%d = %.2f\n", x, i, mezivysledek);
    }
}