#include <stdio.h>

int main(){
    int c;
    if (((c = getchar()) >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9')){
        printf("alfanumericky znak\n\n");
    } 
    else {
        fprintf(stdout, "interpunkcni znak\n\n");
    }
    return 0;
}