#include <stdio.h>

int main()
{
    int i = 5, j = 2;
    float f = 0.0f;

    f = (float) i / (float) j;
    printf("i(%d) / j(%d) = %.1f\n\n", i, j, f);

    return 0;
}