#include <stdio.h>
#include <ctype.h>

int main(){
    char c;

    printf("zadejte znak: ");
    c = getchar();
    getchar();
    if (isdigit(c)){
        printf("číslice: %c\n\n", c);
    }
    else if (ispunct(c)){
        printf("interpunkční znak: %c\n\n", c);
    }
    else if (islower(c)){
        printf("malé písmeno: %c\n\n", c);
    }
    else if (isupper(c)){
        printf("velké písmeno: %c\n\n", c);
    }

    return 0;
}