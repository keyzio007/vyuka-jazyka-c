#include <stdio.h>

int main(){
    int a = 2, b = 2, c = 1, d = 0, e = 4;

    printf("a = 2, b = 2 c = 1, d = 0, e = 4\n\n");
    printf("a++ / ++c * --e = %d\n", (a++ / ++c * --e));
    printf("--b * c++ - a = %d\n", (--b *c++ - a));
    printf("-b - --c = %d\n", (-b - --c));
    printf("++a - --e = %d\n", (++a - --e));
    printf("e / --a * b++ / c++ = %d\n", (e / --a * b++ / c++));
    //printf("e / --a * b++ / c++ = %d\n", (--b *c++ - a));

    return 0;
}