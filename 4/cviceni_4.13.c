#include <stdio.h>

int main(){
    double f = 0.0, g = 0.0, h = 0.0;


    printf("Program, ktery precte 3 double cisla a vypise jejich aritmeticky prumer\n");

    printf("zadejte tri realna ciala: ");
    scanf("%lf%lf%lf", &f, &g, &h);
    printf("aritmeticky prumer cisel %.2f, %.2f a %.2f na dve desetinna cisla je %.2f\n\n", f, g, h, ((f + g + h) / 3));
    putchar('\n');
    putchar('\n');
    return 0;
}