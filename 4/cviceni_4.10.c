#include <stdio.h>

int main(){
    int signed_int = 0;
    unsigned int unsigned_int = 0;
    unsigned_int -= 1;
    signed_int = unsigned_int / 2;

    printf("Program pro zobrazeni maximalni hodnoty unsigned int a signed int\n\n");
    printf("max hodnosta signed int je %d\n", signed_int);
    printf("max hodnosta unsigned int je %u\n", unsigned_int);
    return 0;
}