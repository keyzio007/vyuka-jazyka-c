#include <stdio.h>


int main(){
    double hodnota = 123.456;
    int prevod = 0;

    printf("1. Cela cast cisla %f je %d\n", hodnota, (int) hodnota);
    prevod = hodnota;
    printf("1. Cela cast cisla %f je %d\n", hodnota, prevod);
    printf("3. Cela cast cisla %f je %.0f\n\n", hodnota, hodnota);


    return 0;
}