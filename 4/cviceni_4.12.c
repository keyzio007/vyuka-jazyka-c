#include <stdio.h>

int main(){
    int a = 5, b = 2;
    double f = 3.14, g = 1.2;


    printf("Program pro vyjadreni vyznamu deleni a modulo\n\n");
    printf("int(%d) / int(%d) = %d\n", a, b, (a / b));
    printf("int(%d) / double(%.2f) = %.1f\n", a, g, (a / g));
    printf("double(%.2f) / double(%.2f) = %.1f\n", f, g, (f / g));
    printf("int(%d) %% int(%d) = %d\n", a, b, (a % b));
    putchar('\n');
    putchar('\n');
    return 0;
}