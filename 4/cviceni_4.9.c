#include <stdio.h>

int main(){
    char a, b, c;

    printf("Program pro prevod 3 velkych pismen na mala\nv obracenem poradi\n\n");
    printf("zadejte tri mala pismena:");
    scanf("%c %c %c", &a, &b, &c);
    printf("vase mala pismena jsou jako velka: %c %c %c\n\n", (c + ('a' - 'A')), (b + ('a' - 'A')), (a + ('a' - 'A')));
    return 0;
}