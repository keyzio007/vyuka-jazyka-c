#include <stdio.h>

int main(){
    int cislo;

    printf("Zadejte cislo: ");
    scanf("%d", &cislo);
    printf("Cislo %d je hexadecimalne %x\n", cislo, cislo);
    
    return 0;
}