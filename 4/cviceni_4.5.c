#include <stdio.h>

int main(){
    int a, b = 0;

    printf("Vypocet obsahu obdelnika\n\n");
    printf("zadejte strany obdelnika: ");
    scanf("%d%d", &a, &b);
    printf("obsah obdelnika o stranach %d a %d je %d\n\n", a, b, (a * b));

    return 0;
}