#include <stdio.h>
const double PI = 3.14;

int main(void){
    FILE *fw = NULL;

    if ((fw = fopen("CISLA.TXT", "wt")) == NULL){
        fprintf(stderr, "nepodarilo se otevrit soubor CISLA.TXT\n\n");
        return 1;
    }
    int pocet;
    for (pocet = 1 ; pocet <= 20 ; pocet++){
        putc('$', fw);
        fprintf(fw, "%.2lf", PI * pocet);
        putc('\n', fw);
    }
    return 0;
}