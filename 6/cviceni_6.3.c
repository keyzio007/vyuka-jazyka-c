#include <stdio.h>

int main(){
    int znak;
    FILE *fr = NULL;

    if ((fr = fopen("PISMENA.TXT", "rt")) == NULL){
        fprintf(stderr, "nepodarilo se otevrit souboru PISMENA.TXT\n\n");
        return 1;
    }
    unsigned int pocet  = 0;
    while (znak = getc(fr) != EOF){
        pocet++;
    }
    printf("soubor PISMENA.TXT má %u znaku\n\n", pocet);
    if (fclose(fr) == EOF){
        fprintf(stderr, "nepodarilo se zavrit soubor PISMENA.TXT\n\n");
        return 1;
    }
    return 0;
}