#include <stdio.h>
#include <ctype.h>

int main(void){
    FILE *fr = NULL;
    FILE *fw = NULL;
    int znak = 0;

    if ((fr = fopen("./PISMENA.TXT", "rt")) == NULL){
        fprintf(stderr, "nepodarilo se otevrit soubor PISMENA.TXT\n\n");
        return 1;
    }
    if ((fw = fopen("./VELKY.TXT", "wt")) == NULL){
        fprintf(stderr, "nepodarilo se vytvorit a otevrit soubor VELKY.TXT\n\n");
        if (fclose(fr) == EOF){
            fprintf(stderr, "nepodarilo se zavrit soubor PISMENA.TXT\n\n");
            return 1;
        }
        return 1;
    }
    while ((znak = getc(fr)) != EOF){
        putc(znak, stdout);
        if (isalpha(znak)){
            if (islower(znak)){
                putc(toupper(znak), fw);
            }
            else{
                putc(znak, fw);
            }
        }
        else{
            putc(znak, fw);
        }
        
    }
    return 0;
}