#include <stdio.h>

int main(){
    int znak = 0;
    FILE *fr = NULL;

    if ((fr = fopen("ZNAKY.TXT", "rt")) == NULL){
        fprintf(stderr, "nepodařilo se otevřít souboru ZNAKY.TXT\n\n");
        return 1;
    }
    do
    {
        znak = getc(fr);
        putchar(znak);
    } while (znak != 'q');

    if (fclose(fr) == EOF){
        fprintf(stderr, "nepodařilo se zavřít souboru ZNAKY.TXT\n\n");
        return 1;
    }
 
    return 0;
}