#include <stdio.h>

int main(){
    FILE *fr = NULL;
    double cislo = 0;
    int pocet = 0;
    double vysledek = 0;

    if((fr = fopen("CISLA.TXT", "rt")) == NULL){
        fprintf(stderr, "nepodarilo se otevrit souboru CISLA.TXT\n\n");
        return 1;
    }
    while ((-1) != fscanf(fr," $%lf\n", &cislo)){
        vysledek += cislo;
        pocet++;
    }
    printf("aritmeticky prumer nactenych cisel je %.2f\n\n", (vysledek / pocet));
    return 0;
}